const vscode = require('vscode');
const sidebar = require('./sidebar');

class GitEventManager {
  constructor() {
    const gitExtension = vscode.extensions.getExtension('vscode.git').exports;
    this.api = gitExtension.getAPI(1);
  }

  registerEvent() {
    const subscription = this.api.repositories[0].state.onDidChange(this.change);
  }

  deregisterEvent() {
    this.subscription.dispose();
  }

  change() {
    sidebar.refresh();
  }
}

exports.GitEventManager = GitEventManager;
